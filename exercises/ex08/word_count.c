/* Word Count

Creates a histogram of the words piped in to STDIN
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>

// #define DEBUG
#define MAX_LINE_LEN 256

static void print_histogram_data(gpointer key, gpointer value, gpointer user_data){
    printf("%d %s\n", GPOINTER_TO_INT(value), (char *)key);
}

void free_data (gpointer data){
    #ifdef DEBUG
    printf ("freeing: %s %p\n", (char *) data, data);
    #endif
    free (data);
}

static void clean_word(char ** word){
    //removes trailing punctuation
    // Todo: remove leading quotation marks
    int i = 1;
    char* punctuation = ":-;/.,!?\'\"\n\0";
    int punctuation_num = 12;

    // Remove first leading punctuation character
    for(int x=0; x < punctuation_num; x++){
        if((*word)[0] == punctuation[x]){
            *word = &(*word)[1];
            break;
        }
    }
    
    // Truncate string at first punctuation character
    while(1){
        for(int x=0; x < punctuation_num; x++){
            if((*word)[i] == punctuation[x]){
                // #ifdef DEBUG
                // printf("found %c at %d", (*word)[i], i);
                // #endif
                (*word)[i] = 0;
                return;
            }
        }
        i++;
    }
}

int main(int argc, char *argv[]){
    char *word;
    char in_buf[MAX_LINE_LEN];
    char *edit_buf;

    GHashTable *histogram = g_hash_table_new_full(g_str_hash, g_str_equal, free_data, NULL);
    while(fgets(in_buf, MAX_LINE_LEN, stdin) != NULL) {                      // Read stdin line-by-line
        edit_buf = in_buf;
        while ((word = strsep(&edit_buf, " ")) != NULL){
            #ifdef DEBUG
            printf("%p %p %p %s\n", word, edit_buf, in_buf, word);
            #endif

            clean_word(&word);

            #ifdef DEBUG
            printf("cleaned: %s\n", word);
            #endif
            
            char * key = strdup (word);
            int value = GPOINTER_TO_INT(g_hash_table_lookup(histogram, key));
            
            value += 1;

            g_hash_table_insert(histogram, key, GINT_TO_POINTER(value));
        }
    }

    // printf("Histogram:\n");
    g_hash_table_foreach (histogram, print_histogram_data, NULL);

    return 1;
}
