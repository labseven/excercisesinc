/* shtee.c - Reimplemented tee
  by Adam Novotny

  -a append to file
  -p diagnose errors writing to non pipes
 */

/*
3. For me the biggest issue was figuring out what tee actually does.
Since I did not know what tee does, I had to try it with different input to
know how to reimplement it properly.

Starting with the most basic version and working up made it quite managable.

4. Compared to the linux or macos implementation, my code has a lot less error
checking. Their versions are ~150 and ~250 LoC, which is 2-3x my version.

The linux version uses xnmalloc which is pretty exciting, though I read that
using xmalloc (and its siblings) is bad (not idiomic C).

 */

#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>


// Globals
FILE** out_files;
int num_files;

void cleanup(){
  for(int index = 0; index < num_files; index++)
    fclose(out_files[index]);

  free(out_files);
}

int main(int argc, char* argv[]){
  FILE* fd;
  char buf[1];

  int append_files = 0;
  int ignore_interrupts = 0;
  int c;



  while ((c = getopt (argc, argv, "api:")) != -1)
    switch (c)
      {
      case 'i':
        ignore_interrupts = 1;
        break;
      case 'a':
        append_files = 1;
        break;
      case '?':
        return 1;
      default:
        abort ();
      }

  if(ignore_interrupts)
    signal(SIGINT, SIG_IGN);

  if(argc > optind) {
    num_files = argc - optind;
    out_files = (FILE **) malloc(sizeof(FILE*) * num_files);
    for(int index = 0; index < num_files; index++)
      if(append_files){
        out_files[index] = fopen(argv[index + optind], "a");
      } else {
        out_files[index] = fopen(argv[index + optind], "w");
      }
  }

  while(read(0, buf, sizeof(buf))>0){
    printf("%c", buf[0]);
    for(int index = 0; index < num_files; index++)
      fputc(buf[0], out_files[index]);
  }

  cleanup();
  return(0);
}
