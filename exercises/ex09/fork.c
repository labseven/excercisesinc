/* Example code for Exercises in C.

Copyright 2016 Allen B. Downey
License: MIT License https://opensource.org/licenses/MIT

*/

/*
    Notes:
    After fork() the OS copies the variables for the new process.
    Each process increments the heap_ stack_ and global_shared_value variables, but each process prints out '1' for these. If they shared state, I would expect the value to increment.
    On most OSs, this is done with copy-on-write for performance reasons.

    Thread diagram:
main                                                      
  |                                                       
"creating child 0"                                        
  |                                                       
  |-----------------------------------|                   
"creating child 1"                    |                   
  |                                   |                   
  |----------------------------|      |                   
"creating child 2"             |      |                   
  |                            |      |                   
  |---------------------|      |      |                   
  |                     |      |      |                   
"hello from parent"     |      |      |                   
  |                     |      |      |                   
  |                     |      |      |                   
  |                     |      |     sleep 0              
  |                     |      |      |                   
  |                     |      |     "hello from child 0"
wait<__________________ |  ___ | ____exit 0               
  |                     |      |                          
  |                     |      |                          
  |                     |     sleep 1                     
  |                     |      |                          
  |                     |     "hello from child 1"        
  |                     |      |                          
wait<__________________ | ____exit 1                      
  |                     |                                 
  |                    sleep 1                            
  |                     |                                 
  |                    "hello from child 2"               
  |                     |                                 
wait<__________________exit 2   
  |
exit 0  
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <wait.h>


// errno is an external global variable that contains
// error information
extern int errno;
int global_shared_value;

int main(int argc, char *argv[]);

// get_seconds returns the number of seconds since the
// beginning of the day, with microsecond precision
double get_seconds() {
    struct timeval tv[1];

    gettimeofday(tv, NULL);
    return tv->tv_sec + tv->tv_usec / 1e6;
}


void child_code(int i)
{
    sleep(i+1);
    printf("\n\nHello from child %d.\n", i);

    global_shared_value += 1;

    printf("global_shared_value: %i (%p)\n", global_shared_value, &global_shared_value);
    printf("address of main: %p\n", &main);
}

// main takes two parameters: argc is the number of command-line
// arguments; argv is an array of strings containing the command
// line arguments
int main(int argc, char *argv[])
{
    int status;
    pid_t pid;
    double start, stop;
    int i, num_children;

    int stack_shared_value = 0;
    global_shared_value = 0;
    int* heap_shared_value = malloc(sizeof(int));
    *heap_shared_value = 0;

    // the first command-line argument is the name of the executable.
    // if there is a second, it is the number of children to create.
    if (argc == 2) {
        num_children = atoi(argv[1]);
    } else {
        num_children = 1;
    }

    // get the start time
    start = get_seconds();

    for (i=0; i<num_children; i++) {

        // create a child process
        printf("Creating child %d.\n", i);
        pid = fork();

        /* check for an error */
        if (pid == -1) {
            fprintf(stderr, "fork failed: %s\n", strerror(errno));
            perror(argv[0]);
            exit(1);
        }

        /* see if we're the parent or the child */
        if (pid == 0) {
            child_code(i);

            heap_shared_value += 1;
            stack_shared_value += 1;

            printf("child %d: stack_shared_value: %d\n", i, stack_shared_value);
            printf("child %d: heap_shared_value: %i (%p)\n", i, *heap_shared_value, heap_shared_value);

            exit(i);
        }
    }

    /* parent continues */
    printf("\n\nHello from the parent.\n");

    global_shared_value += 1;
    stack_shared_value += 1;
    heap_shared_value += 1;

    printf("parent global_shared_value: %i (%p)\n", global_shared_value, &global_shared_value);
    printf("parent stack_shared_value: %d\n", stack_shared_value);
    printf("parent heap_shared_value: %i (%p)\n", *heap_shared_value, heap_shared_value);

    printf("address of main: %p\n", &main);


    for (i=0; i<num_children; i++) {
        pid = wait(&status);

        if (pid == -1) {
            fprintf(stderr, "wait failed: %s\n", strerror(errno));
            perror(argv[0]);
            exit(1);
        }

        // check the exit status of the child
        status = WEXITSTATUS(status);
        printf("Child %d exited with error code %d.\n", pid, status);
    }
    // compute the elapsed time
    stop = get_seconds();
    printf("Elapsed time = %f seconds.\n", stop - start);

    exit(0);
}
