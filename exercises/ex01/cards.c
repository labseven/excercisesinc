/*
  Card counting software. Input visible cards, and it will print out current strategy.
 */

#include <stdio.h>
#include <stdlib.h>

/* Prompts the user for a card and puts the reply into the given buffer.
  User input is truncated to the first two characters.

  card_name: buffer where input is stored
 */
void user_input(char *card_name) {
  puts("Enter the card: ");
  scanf("%2s", card_name);
}

/* Given a card name, returns the value of the card.

  card_name: the name of the card
*/
int card_value(char* card_name) {
  int val = 0;
  printf("%c\n", *card_name);
  switch(card_name[0]) {
    case 'K':
    case 'Q':
    case 'J':
      val = 10;
      break;
    case 'A':
      val = 11;
      break;
    default:
      val = atoi(card_name);
      /* sentinel values are bad, but not validating input is worse */
      if ((val < 1) || (val > 10)) {
        return -1;
    }
  }
  return val;
}

/* Keeps a count of high and low cards. Returns the current count.

  card_val: value of a new card
 */
int update_count(int card_val) {
  static int count = 0;
  if ((card_val > 2) && (card_val < 7)) {
    count++;
  } else if (card_val == 10) {
    count--;
  }
  return count;
}

int main()
{
  char input[3];
  int card_val;
  do {
    user_input(input);

    if (input[0] == 'X') { continue; }
    card_val = card_value(input);
    if (card_val == -1) {
      puts("I don't understand that value!");
      continue;
    }

    printf("Current count: %i\n", update_count(card_val));
  } while (*input != 'X');

  return 0;
}
