#include <stdio.h>

/* If the declaration is here, x is global and needs to be declared in the
assembly at the end (even if stays constant). Probably so that it can be used
in other files. */
//int x = 5;
int main() {
  // int x = 5;
  // /* if x is not used, then in optimization 2 it is completely removed */
  // printf("Hello, World!\n");
  //  /* when printing this with -O2, x is turned into a constant (movl $5, %esi) */
  // printf("Hello, World!\n%i\n", x);

  // /* with -O2 this both x and y are turned into a const, (movl $7, %esi) */
  // int y = x + 2;
  // printf("Hello, %i\n", y);
  //
  int a = 3;
  int b = 4;
  int c = a + b;
  printf("c is %d\n", c);
  if (c%2 == 0) {
    printf("c is even\n");
  } else {
    printf("c is odd\n");
  }

  return 0;
}
