	.arch armv5te
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 6
	.eabi_attribute 34, 0
	.eabi_attribute 18, 4
	.file	"hello.c"
	.text
	.section	.rodata
	.align	2
.LC0:
	.ascii	"c is even\000"
	.align	2
.LC1:
	.ascii	"c is odd\000"
	.text
	.align	2
	.global	main
	.syntax unified
	.arm
	.fpu softvfp
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #16
	mov	r3, #3
	str	r3, [fp, #-8]
	mov	r3, #4
	str	r3, [fp, #-12]
	ldr	r2, [fp, #-8]
	ldr	r3, [fp, #-12]
	add	r3, r2, r3
	str	r3, [fp, #-16]
	ldr	r3, [fp, #-16]
	and	r3, r3, #1
	cmp	r3, #0
	bne	.L2
	ldr	r3, .L5
.LPIC0:
	add	r3, pc, r3
	mov	r0, r3
	bl	puts(PLT)
	b	.L3
.L2:
	ldr	r3, .L5+4
.LPIC1:
	add	r3, pc, r3
	mov	r0, r3
	bl	puts(PLT)
.L3:
	mov	r3, #0
	mov	r0, r3
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, pc}
.L6:
	.align	2
.L5:
	.word	.LC0-(.LPIC0+8)
	.word	.LC1-(.LPIC1+8)
	.size	main, .-main
	.ident	"GCC: (Debian 8.2.0-11) 8.2.0"
	.section	.note.GNU-stack,"",%progbits
