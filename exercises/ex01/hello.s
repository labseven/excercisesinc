	.arch armv5te
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 34, 0
	.eabi_attribute 18, 4
	.file	"hello.c"
	.text
	.section	.text.startup,"ax",%progbits
	.align	2
	.global	main
	.syntax unified
	.arm
	.fpu softvfp
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r0, .L4
	push	{r4, lr}
	mov	r1, #7
.LPIC0:
	add	r0, pc, r0
	bl	printf(PLT)
	ldr	r0, .L4+4
.LPIC1:
	add	r0, pc, r0
	bl	puts(PLT)
	mov	r0, #0
	pop	{r4, pc}
.L5:
	.align	2
.L4:
	.word	.LC0-(.LPIC0+8)
	.word	.LC1-(.LPIC1+8)
	.size	main, .-main
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"c is %d\012\000"
	.space	3
.LC1:
	.ascii	"c is odd\000"
	.ident	"GCC: (Debian 8.2.0-11) 8.2.0"
	.section	.note.GNU-stack,"",%progbits
