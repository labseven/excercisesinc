numbers = set()
for c in range(9):
    for e in [1,2,3]:
        numbers.add(c*10**e)

for x in sorted(numbers):
    print(x)