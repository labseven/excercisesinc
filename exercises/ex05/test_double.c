#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "rand.h"

int main (int argc, char *argv[])
{
    int i;
    double x;

    srandom (time (NULL));

    // const int num_trials = 2;
    const int num_trials = 10000;


    for (i=0; i<num_trials; i++) {
        x = my_random_double();
        printf ("%lf\n", x);
    }

    return 0;
}
